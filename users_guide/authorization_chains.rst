Authorization Chains
====================

.. currentmodule:: ferris.core.controller

Ferris uses the concept of authorization chains to control access to :doc:`controllers <controllers>` and their actions. The concept is simple: an authorization chain consists of a series of functions (or callables). When trying to determine to allow or deny a request, each function in the chain is called. If any of the functions return False then the request is rejected. Chains are specified using :attr:`Controller.Meta.authorizations <ferris.core.controller.Controller.Meta.authorizations>` and the :func:`add_authorizations` decorator.


Using Authorizations
--------------------

Here's an example of requiring a user to be logged in to access a controller::

    from ferris import Controller

    def require_user(controller):
        return True if controller.user else False

    class Protected(Controller):
        class Meta:
            authorizations = (require_user,)

        ...

You can also use :func:`add_authorizations` instead::

    @route
    @add_authorizations(require_user)
    def protected_action(self):
        ...

.. autofunction:: add_authorizations


To add authorizations globally, use the :doc:`global event bus <events>`.


Creating Authorization Functions
--------------------------------

As shown above, a simple authorization function is rather trivial::

    def require_user(controller):
        return True if controller.user else False


Note that you can also include a message::

    def require_user(controller):
        return True if controller.user else (False, "You must be logged in!")

Or if you'd like to redirect or present your own error page, you can return any valid response::

    def require_user(controller):
        if controller.user:
            return True
        url = users.create_login_url(dest_url=controller.request.url)
        return controller.redirect(url)


Using Authorization Chains with Scaffolding
-------------------------------------------

By default, scaffolded actions are not protected and are accessable to anyone with the instance key. Take care when exposing public facing actions to protect any sensitive data. Please use authorization chains where necessary and roll your own security model to protect any data that requires protection.

The example below wraps certain actions with a layer of protection that checks if the calling user has access to the requested data. The implementation of has_access is left up to the developer.

    def check_access_first(delegate):
        def inner(self, key, *args, **kwargs):
            item = self.util.decode_key(key)
            if not item:
                return 404
            if not item.has_access(self.user):
                return 401
            return delegate(self, key, *args, **kwargs)
    return inner

    secure_edit = check_access_first(scaffold.edit)
    secure_view = check_access_first(scaffold.view)
    secure_delete = check_access_first(scaffold.delete)

If the calling user has access to the data, the delegated function is called and Ferris proceeds as normal. Notice that the function returns 404 if the item doesn't exist and 401 if the user does not have access as determined by has_access.


Built-in Authorization Functions
--------------------------------

.. module:: ferris.core.auth

The module ``ferris.core.auth`` includes some built-in useful authorization functions and utilities.

.. autofunction:: require_user
.. autofunction:: require_admin


There are a few function generators that use predicates. These are useful shortcut authorization functions.

.. autofunction:: require_user_for_prefix(prefix)

    Generates an authorization function that requires that users are logged in for the given prefix.

.. autofunction:: require_admin_for_prefix(prefix)

    Generates an authorization function that requires that the user is an App Engine admin for the given prefix.

.. autofunction:: require_user_for_action
.. autofunction:: require_admin_for_action
.. autofunction:: require_user_for_route
.. autofunction:: require_admin_for_route


You can also create your own generators using precidates.

.. autofunction:: prefix_predicate
.. autofunction:: action_predicate
.. autofunction:: route_predicate

Then use ``predicate_chain`` to combine them with your authorization function.

.. autofunction:: predicate_chain
